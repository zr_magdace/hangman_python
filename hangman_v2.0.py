# -*- coding: utf-8 -*-
"""
Created on Sun Sep  3 12:13:26 2023

@author: Magda
"""
import random 
import time

word_list=["PRZYGODA", "DESKOROLKA" , "PARASOLKA", "WAKACJE"]

gallows=[" ", 
""" 
  
     
     

 
 
/ \         
""",
""" 
  
 |  
 |     
 |        
 |       
 |       
/ \         
""",
""" 
  
 |  /     
 | /          
 |        
 |       
 |       
/ \         
""" ,
""" 
  ---------
 |  /     
 | /         
 |        
 |       
 |       
/ \         
""",
""" 
  ---------
 |  /     |
 | /      |    
 |        
 |       
 |       
/ \         
""",
""" 
  ---------
 |  /     |
 | /      |    
 |        O
 |       /|\\
 |       / \\
/ \         
"""       
]

def print_intro():
    print("************************")
    print("*******GRA WISIELEC*****")
    print("************************")
    
def select_word():
    word=random.choice(word_list)
    return str(word)

def create_space(word):
    lw=len(word)
    space=list("_"*lw)
    print(f"Twoje słwo składa się z {lw} liter")
    print(' '.join(space)) 
    print("        ")
    return  space
    
def check_letter(letter, word, space):   
    for i in range(len(word)):
        if word[i]==letter:
           space[i]=letter 
    space_letter=' '.join(space)     
    print(space_letter)

def count_letter(letter, word):   
    counterL=0
    for i in range(len(word)):
        if word[i]==letter:
           counterL=counterL+1
    return float(counterL)
    
def count_letter_in_space(space):
    counterS=0
    for i in range(len(space)):
        if space[i]!='_':
            counterS=counterS+1   
    return float(counterS)

def stop_play(counterS, word):
    if counterS==len(word):
        stop=True
        print("           ")
        print ("********** WYGRAŁEŚ !!! *******")
    if counterS!=len(word):
        stop=False
    return stop
     
def draw_gallows(mistake, counterL): 
   if counterL==0:
        print("           ")
        if mistake<6:
            print("ZłY WYBÓR") 
            print(gallows[mistake])
        elif mistake==6:
            print("******WISISZ******") 
            print(gallows[mistake])
   else:
       print("           ")
       print("DOBRY WYBÓR")
       print("           ")
    
def count_mistake(counterL): 
   if counterL==0:
        add=1
   if counterL!=0:
       add=0
   return int(add) 


def play(): 
    print_intro()
    word=select_word()
    space=create_space(word)
    mistake=0
    stop=False
    while mistake<6 and stop==False:
        letter=input("Podaj literę: ").upper()
        counterL=count_letter(letter, word)   
        check_letter(letter, word, space)
        counterS=count_letter_in_space(space)
        mistake=mistake+count_mistake(counterL)
        draw_gallows(mistake, counterL)
        stop=stop_play(counterS, word)
        time.sleep(1)        
        #print (stop)   #do spr
        #print (mistake)
        
def play_again (decission):
    decission=input("Czy grasz dalej (t/n): ")
    if decission=="t":
        play()
    else:
       print("********KONIEC*********")   

def main():
    decission="t"
    play()
    play_again(decission)
    
main()       




